﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewsAPIProject.Models;
using Newtonsoft.Json;

namespace NewsAPIProject.Controllers
{
    public class HomeController : Controller
    {
        List<News> news;
        public readonly NewsContext context;

        public ViewResult Index()
        {
            ViewBag.Title = "Home";
            ViewBag.Date = DateTime.Now;
            ViewBag.ActivePage = "Home";

            return View();
        }

        public ViewResult News()
        {
            ViewBag.Title = "News";
            ViewBag.Date = DateTime.Now;
            ViewBag.ActivePage = "News";

            return View(news);
        }

        public ViewResult Comment()
        {
            ViewBag.Title = "Comment";
            ViewBag.Date = DateTime.Now;
            ViewBag.ActivePage = "Comment";

            return View();
        }

        public HomeController(NewsContext newsContext)
        {
            context = newsContext;

            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format("https://newsapi.org/v2/top-headlines?sources=fox-news&apiKey=615b2ff56be646b98a05f28154f60c0e"));

            webReq.Method = "GET";

            HttpWebResponse webResp = (HttpWebResponse)webReq.GetResponse();

            Debug.WriteLine(webResp.Server);
            Debug.WriteLine(webResp.StatusCode);

            string jsonString;
            string str;
            using (Stream stream = webResp.GetResponseStream())
            {
                StreamReader sr = new StreamReader(stream, System.Text.Encoding.UTF8);
                jsonString = sr.ReadToEnd();
                int i = jsonString.IndexOf("[") + 2;
                str = jsonString.Substring(i);
                str = str.Remove(str.Length - 1);
                str = str.Insert(0, "[{");
                str = str.Replace("\"id\"", "\"newssource\"");
                Debug.WriteLine(str);
            }
            news = null;
            news = JsonConvert.DeserializeObject<List<News>>(str);
            for (int i = 0; i < 10; i++)
            {
                context.News.Add(news[i]);
            }
            context.SaveChanges();
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<News> GetAllNews()
        {
            return context.News.ToList();
        }


    }
}