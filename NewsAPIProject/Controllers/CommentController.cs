﻿using System.Collections.Generic;
using NewsAPIProject.Models;
using NewsAPIProject.Services;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace BooksApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        
        private readonly CommentService _commentService;

        public CommentController(CommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpGet]
        public ActionResult<List<Comments>> Get()
        {
            return _commentService.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetComment")]
        public ActionResult<Comments> Get(string id)
        {
            var comment = _commentService.Get(id);

            if (comment == null)
            {
                return NotFound();
            }

            return comment;
        }

        [HttpPost]
        //public ActionResult<Comments> Create(Comments comment)
        public ActionResult<Comments> Create([FromBody] Comments comment)
        {
            _commentService.Create(comment);

            return CreatedAtRoute("GetComment", new { id = comment.Id.ToString() }, comment);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Comments commentIn)
        {
            var comment = _commentService.Get(id);

            if (comment == null)
            {
                return NotFound();
            }

            _commentService.Update(id, commentIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var comment = _commentService.Get(id);

            if (comment == null)
            {
                return NotFound();
            }

            _commentService.Remove(comment.Id);

            return NoContent();
        }
    }
}