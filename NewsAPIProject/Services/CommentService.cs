﻿using System.Collections.Generic;
using System.Linq;
using NewsAPIProject.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace NewsAPIProject.Services
{
    public class CommentService
    {
        private readonly IMongoCollection<Comments> _comments;

        public CommentService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("CommentsDB"));
            var database = client.GetDatabase("CommentsDB");
            _comments = database.GetCollection<Comments>("comment");
        }

        public List<Comments> Get()
        {
            return _comments.Find(comment => true).ToList();
        }

        public Comments Get(string id)
        {
            var docId = new ObjectId(id);

            return _comments.Find<Comments>(comment => comment.Id == docId).FirstOrDefault();
        }

        public Comments Create(Comments comment)
        {
            _comments.InsertOne(comment);
            return comment;
        }

        public void Update(string id, Comments commentIn)
        {
            var docId = new ObjectId(id);

            _comments.ReplaceOne(comment => comment.Id == docId, commentIn);
        }

        public void Remove(Comments commentIn)
        {
            _comments.DeleteOne(comment => comment.Id == commentIn.Id);
        }

        public void Remove(ObjectId id)
        {
            _comments.DeleteOne(comment => comment.Id == id);
        }
    }
}