﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsAPIProject.Hubs
{
    public class CommentHub: Hub
    {
        public async Task SendData(string user, string message, string id)
        {
            await Clients.All.SendAsync("CommentToDB", user, message, id);
        }
    }
}
