﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/commentHub").build();


window.addEventListener('load', function () {
    //var div = document.createElement("div");
    var jsonString = JSON.parse(httpGet('http://localhost:5000/api/comment'));
    for (var i = 0; i < Object.keys(jsonString).length; i++) {//maximum 50 berichten
        var div = document.createElement("div");
        this.console.log((jsonString[i] ? jsonString[i].newsId : "") + (jsonString[i] ? jsonString[0].name : "") + (jsonString[i] ? jsonString[i].message : ""));
        var t = document.createTextNode((jsonString[i] ? jsonString[0].name : "") + " commented on article id: " + (jsonString[i] ? jsonString[i].newsId : "") + ": " + (jsonString[i] ? jsonString[i].message : ""));
        document.getElementById("messagesList").appendChild(div).appendChild(t);
    }
}, false);

function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}