﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/commentHub").build();

connection.start().catch(function (err) {
    return console.error(err.toString());
});
connection.on("CommentToDB", function (user, message, id) {
    httpPost('http://localhost:5000/api/comment', id, user, message);
});
document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    var id = document.getElementById("idInput").value;
    alert("Comment added!");
    connection.invoke("SendData", user, message, id).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});
function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}
function httpPost(theUrl, newsId, Name, Message) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", theUrl, false); // false for synchronous request
    xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttp.send("{\"NewsId\":\""+ newsId +"\",\"Name\":\""+ Name +"\",\"Message\":\""+ Message +"\"}");
    return xmlHttp.responseText;
}
