﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NewsAPIProject.Models
{
    public class Comments
    {
        public ObjectId Id { get; set; }

        [BsonElement("NewsId")]
        public string NewsId { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Message")]
        public string Message { get; set; }

    }
}
