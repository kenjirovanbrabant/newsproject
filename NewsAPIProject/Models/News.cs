﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewsAPIProject.Models
{
    public class News
    {
        [Key]
        public uint id { get; set; }
        public Source source { get; set; }
        public string author { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public string urlToImage { get; set; }
        public string publishedAt { get; set; }
        public string content { get; set; }
    }

    public class Source
    {
        public uint id { get; set; }
        public string newssource { get; set; }
        public string name { get; set; }
    }
}
